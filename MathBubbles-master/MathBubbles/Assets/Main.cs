﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class Main : MonoBehaviour {

	private const int MAX = 16;
	private const int MIN = 5;

	public Sprite sprite;
    public TextMesh equation;
    public TextMeshProUGUI timeLeft;
    public TextMeshProUGUI currScore;
    public TextMeshProUGUI currLevel;
    public TextMeshProUGUI currAnswer;
    

	public Sprite[] sprites;
	private string answer;
	private ArrayList shapes;
    //private Color[] colors = new Color[] { Color.blue, Color.green, Color.yellow, Color.red };
    private Color lightBlue = new Color(0.51f, 0.823f, 1.0f);
    private Color lightGreen = new Color(0.0f, 1.0f, 0.066f);
    //private Color lightYellow = new Color(0.51f, 0.823f, 1f);
    private Color lightRed = new Color(1.0f, 0.49f, 0.411f);

    private Color[] colors = new Color[] { new Color(0.51f, 0.823f, 1.0f), new Color(0.0f, 1.0f, 0.066f), new Color(1.0f, 0.49f, 0.411f), Color.yellow };
    private System.Random rand;
	private int level;
	private int score;
	private float time;
    private RandomTime bonusTimeScript;
	// Number of correct or incorrect consecutive answers
	// If positive, a number of correct answers has been detected
	// If negative, a number of incorrect answers has been detected
	private int consecutive;

    void Awake()
    {
        float width = currScore.GetPreferredValues().x;
        float height = currScore.GetPreferredValues().y;
        Vector3 scorePos = new Vector3(0 + width, Screen.height - height, 0);
        //scorePos = Camera.main.ScreenToWorldPoint(scorePos);
        //scorePos.x += 100;
        //scorePos.y -= 100;
        currLevel.SetText("Level:\n" + level);
        currScore.transform.position = scorePos;
        Vector3 levelPos = new Vector3(0 + currLevel.GetPreferredValues().x, 0 + currLevel.GetPreferredValues().y, 0);
        currLevel.transform.position = levelPos;
    }

    // Use this for initialization
    void Start () {
        bonusTimeScript = null;
        level = 1;
		score = 0;
        time = 90f;
        answer = "0";
        
        currLevel.SetText("Level\n" + level);
        Vector3 levelPos = new Vector3(0 + currLevel.GetPreferredValues().x / 2, 0 + currLevel.GetPreferredValues().y / 2, 0);
        currLevel.transform.position = levelPos;

        currScore.SetText("Score\n" + score);
        Vector3 scorePos = new Vector3(0 + currScore.GetPreferredValues().x / 2, Screen.height - currScore.GetPreferredValues().y / 2, 0);
        currScore.transform.position = scorePos;

        timeLeft.SetText("Time\n" + time);
        Vector3 timePos = new Vector3(Screen.width - timeLeft.GetPreferredValues().x / 2, Screen.height - timeLeft.GetPreferredValues().y / 2, 0);
        timeLeft.transform.position = timePos;

        currAnswer.SetText("Answer\n" + answer);
        Vector3 ansPos = new Vector3(Screen.width - currAnswer.GetPreferredValues().x / 2, 0 + currAnswer.GetPreferredValues().y / 2, 0);
        currAnswer.transform.position = ansPos;

        PlayerPrefs.SetInt("Score", score);
		consecutive = 0;
		rand = new System.Random ();
		shapes = new ArrayList ();
		CreateShapes ();
        
	}

	private double[] GetRandomPositions() {
		double x = 0.4 * rand.NextDouble () + 0.3;
		double y = 0.5 * rand.NextDouble () + 0.2;
		double[] pos = new double[2];
		pos [0] = x;
		pos [1] = y;
		return pos;
	}

	// Checks if new position passes requirements
	private bool IsNewPositionValid(double[,] positions, double[] newPos) {
		//Debug.Log ("Inside check new position");
		for (int i = 0; i < positions.GetLength (0); i++) {
			double diff1 = Mathf.Abs ((float) (positions [i, 0] - newPos [0]));
			double diff2 = Mathf.Abs ((float) (positions [i, 1] - newPos [1]));

			if (diff1 < 0.2 && diff2 < 0.2)
				return false;
		}
		return true;
	}

	void CreateShapes() {

		answer = GenerateAnswer (level).ToString ();
        currAnswer.SetText("Answer\n" + answer);
		int numberOfShapes = Con.Constants.GetNumberOfQuestions (level);
		double[,] positions = new double[numberOfShapes,2];
        List<Equation> equations = new List<Equation>();


		for (int i = 0; i < numberOfShapes; i++) {
			GameObject shape = (GameObject) Instantiate(Resources.Load("Circle"), new Vector3(i * 4, 0, 0), Quaternion.identity);
			ShapeScript script = shape.GetComponent<ShapeScript> ();

            Color c = colors[rand.Next(colors.Length)];
			script.UploadColor (c);

            int randomSprite = rand.Next(Con.Constants.SPRITE_NAMES.GetLength(0));
            script.UploadSprite (Con.Constants.SPRITE_NAMES[randomSprite]);

			double[] newPos = new double[2];
			newPos = GetRandomPositions ();

			while (!IsNewPositionValid(positions, newPos)) {
				newPos = GetRandomPositions ();
			}


			positions [i, 0] = newPos [0];
			positions [i, 1] = newPos [1];

			script.UploadPosition (newPos [0], newPos [1]);
            
            Equation e = null;
            
            int counter = 0;
            do
            {
                if (i == 0)
                {
                    e = new Equation(level, true, int.Parse(answer));
                    //equations.Add(script.UpdateEqu(level, true, int.Parse(answer)));
                }
                else
                {
                    e = new Equation(level, false, int.Parse(answer));
                    //equations.Add(script.UpdateEqu(level, false, int.Parse(answer)));
                }

                counter++;
            } while (equations.Contains(e));

            script.UpdateEq(e);
            
            equations.Add(e);
            

	
			shapes.Add (shape);
            
		}
	}

	void DestroyShapes() {
		for (int i = 0; i < shapes.Count; i++) {
			GameObject shape = (GameObject) shapes [i];
			Destroy (shape);
		}
		shapes.Clear ();
	}

	// Update is called once per frame
	void Update () {
		time -= Time.deltaTime;
        timeLeft.SetText("Time\n" + (int) time);

        if (time < 0f) {
			Debug.Log ("GAME OVER");
		}

		CheckForHits ();
        //GenerateRandomTimeBonus();
	}

	int GenerateAnswer(int level) {

        int answer = 0;

        do
        {
          answer = rand.Next(Con.Constants.LEVEL_MIN_MAX[level - 1, 0], Con.Constants.LEVEL_MIN_MAX[level - 1, 1]);
        } while (Con.Constants.IsPrime(answer));

        return answer;
	}

	void LevelUpOrDown(GameObject obj) {
		if (obj.GetComponent<ShapeScript> ().IsCorrect()) {
			if (consecutive <= 0) {
				consecutive = 1;
			} else {
				consecutive++;
			}
		} else {
			if (consecutive >= 0) {
				consecutive = -1;
			} else {
				consecutive--;
			}
		}

		if (consecutive == Con.Constants.CONSECUTIVE_POSITIVE) {
            if (level != Con.Constants.MAX_LEVEL)
            {
                level++;
            }
            consecutive = 0;
		}

		if (consecutive == Con.Constants.CONSECUTIVE_NEGATIVE) {
			if (level != 1) {
				level--;
			}
			consecutive = 0;
		}
        currLevel.SetText("Level\n" + level);
        //Debug.Log ("Level Up or Down Finished");
    }

	void UpdateScore() {
		score += 2 * level;
        currScore.SetText("Score\n" + score);
        //Debug.Log ("Update score finished");
    }

    void GenerateRandomTimeBonus()
    {
        int random = rand.Next(100);
        int counter = 0;
        if (time >= 88 && time <= 88.03)
        {   if (counter == 0)
            {
                GameObject bonus = (GameObject)Instantiate(Resources.Load("RandomTime"), new Vector3(0, 0, 0), Quaternion.identity);
                bonusTimeScript = bonus.GetComponent<RandomTime>();
                //randomTime.CreatePositions();
            }
            counter++;
        }
    }

	void CheckForHits() {
		
		if( Input.GetMouseButtonDown(0) )
		{
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
            Vector2 pos = Input.mousePosition;
			RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

			if (hit != null && hit.collider != null) {
				if (hit.transform.gameObject.tag.Equals("Shape")) {
					LevelUpOrDown (hit.transform.gameObject);
					UpdateScore ();
					DestroyShapes ();
					CreateShapes ();
				}

                if (hit.transform.gameObject.tag.Equals("Random"))
                {
                   time += bonusTimeScript.getAdditionalTime(); 
                }
			}
		}
	}
}
