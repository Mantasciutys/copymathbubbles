﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShapeScript : MonoBehaviour {

	public Sprite[] sprites;
	public Sprite sprite;
	private Color color;
	private float velocityX, velocityY;
	private bool right, up;
	private string equation;
	private bool correct;
	private int fontSize;
	private Rect textArea;
	private GUIStyle style;
	private GUIContent content;
	private double shapeLength;
	public Equation eq;
	int counter = 0;
    private TextMeshPro text;

	System.Random random;

	void Awake() {
		random = new System.Random ();
		int rand = random.Next (sprites.Length);
		float floatRandom = Random.Range (Con.Constants.SMALLEST_SHAPE_SIZE, Con.Constants.LARGEST_SHAPE_SIZE);

		if (random.Next (2) == 0) {
			right = true;
		} else {
			right = false;
		}

		if (random.Next (2) == 0) {
			up = true;
		} else {
			up = false;
		}

		velocityX = Random.Range (Con.Constants.VELOCITY_MIN, Con.Constants.VELOCITY_MAX);
		velocityY = Random.Range (Con.Constants.VELOCITY_MIN, Con.Constants.VELOCITY_MAX);
	}

	// Use this for initialization
	void Start () {
		random = new System.Random ();
		int rand = random.Next (sprites.Length);
		float floatRandom = Random.Range (Con.Constants.SMALLEST_SHAPE_SIZE, Con.Constants.LARGEST_SHAPE_SIZE);
		Vector2 temp = transform.localScale;
		temp.x = floatRandom;
		temp.y = floatRandom;
		transform.localScale = temp;

		shapeLength = floatRandom;

        fontSize = (int) (shapeLength * 3);

		if (random.Next (2) == 0) {
			right = true;
		} else {
			right = false;
		}

		if (random.Next (2) == 0) {
			up = true;
		} else {
			up = false;
		}

		velocityX = Random.Range (Con.Constants.VELOCITY_MIN, Con.Constants.VELOCITY_MAX);
		velocityY = Random.Range (Con.Constants.VELOCITY_MIN, Con.Constants.VELOCITY_MAX);
	}

	public void UploadColor(Color c) {
        GetComponent<SpriteRenderer>().color = c;
        color = c;
	}	

    public void UploadTextMeshProText(TextMeshPro t)
    {
        text = t;
        text.SetText("TEXT");
        text.transform.position = new Vector3(50, 50, 0);
    }

	public void UploadSprite(string spriteName) {
        //Debug.Log("Was: " + GetComponent<SpriteRenderer>().sprite.name);
		GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (spriteName);
        //Debug.Log("New: " + GetComponent<SpriteRenderer>().sprite.name);
        //Debug.Log("Sprite uploaded");
    }

	public double UploadFontSize() {
		double firstMultiplier = 0;
		switch (GetComponent<SpriteRenderer>().sprite.name) {
		case "Circle":
			firstMultiplier = 1.3;
			break;
		case "Triangle":
			firstMultiplier = 1;
			break;
		case "Square":
			firstMultiplier = 1.5;
			break;
		}
		return firstMultiplier;
	}

	public Equation GetEquation() {
		return eq;
	}

	public float GetX() {
		return GetComponent<Rigidbody2D> ().position.x;
	}

	public float GetY() {
		return GetComponent<Rigidbody2D> ().position.y;
	}

	public void UploadPosition(double x, double y) {
		Vector3 temp = new Vector3 (0, 0, 1); 
		temp.x = (float) x;
		temp.y = (float) y;

        GetComponent<Rigidbody2D>().position = Camera.main.ViewportToWorldPoint(temp);

        //Debug.Log("Shape pos x: " + GetComponent<Rigidbody2D>().position.x);
        //Debug.Log("Shape pos y: " + GetComponent<Rigidbody2D>().position.y);
    }

	public void UpdateEquation(string e) {
		equation = e;
	}

	public void UpdateEq(Equation e) {
        eq = e;
    }

	public Equation UpdateEqu(int level, bool corr, int answer) {
		eq.ChangeLevel (level);
		eq.ChangeCorrect (corr);
		eq.ChangeAnswer (answer);
		eq.GenerateQuestion ();
        return eq;
	}

	public void UpdateCorrectOrNot(bool c) {
		correct = c;
	}

	public double GetShapeLength() {
		return shapeLength;
	}

	void OnGUI() {
        Vector2 shapePosition = Camera.main.WorldToScreenPoint(transform.position);
        GUIContent gUIContent = new GUIContent(eq.ToString());
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.black;
        Vector2 textSize = GUI.skin.label.CalcSize(gUIContent);
        GUI.Label(new Rect(shapePosition.x - textSize.x / 2, Screen.height - shapePosition.y, textSize.x, textSize.y), eq.ToString(), style);
    }

    private double GetEqLengthProportion() {
		switch(eq.GetLength()) {
		case 1:
		case 2:
		case 3:
		case 4:
			return 1;
		case 5:
			return 0.9;
		case 6:
			return 0.8;
		case 7:
			return 0.6;
		}
		return 1;
	}	

	// Check which way object has to go (true -> right, false -> left)
	bool GoToRight(Vector2 t) {
		if (t.x >= 0.8) {
			right = false;
            //Debug.Log("Go Left: x: " + t.x + ", y: " + t.y);
        } else if (t.x <= 0.2) {
            right = true;
            //Debug.Log("Go Right: x: " + t.x + ", y: " + t.y);
            //Debug.Log("Go Right");
        }
		return right;
	}

	// Check which way object has to go (true -> up, false -> down)
	bool GoUp(Vector2 t) {
		if (t.y >= 0.8) {
            //Debug.Log("Go Down");
            //Debug.Log("Go Down: x: " + t.x + ", y: " + t.y);
            up = false;
		} else if (t.y <= 0.2) {
            up = true;
            //Debug.Log("Go Up: x: " + t.x + ", y: " + t.y);
            //Debug.Log("Go Up");
        }
		return up;
	}

	void OnMouseDown() {
		if (correct) {
			//Debug.Log ("Correct clicked");
		} else {
			//Debug.Log ("incorrect");
		}
	}

	public bool IsCorrect() {
		return eq.GetCorrectness();
	}

	public string GetSpriteName() {
		return sprite.name;
	}
		
	
	// Update is called once per frame
	void Update () {
        Vector2 bodyPosition = GetComponent<Rigidbody2D>().position;
        //Debug.Log("Start position -------------, x: " + bodyPosition.x + ", y: " + bodyPosition.y);
        //Vector2 bodyPosition = transform.position;

        Vector2 viewportPoint = Camera.main.WorldToViewportPoint (bodyPosition);
        
        GoToRight(viewportPoint);
		GoUp (viewportPoint);

        //Debug.Log("Velocity x: " + velocityX + ",y: " + velocityY);
        
		if (up) {
			bodyPosition.y += velocityY * Time.smoothDeltaTime;
            //Debug.Log("UP, value of move: " + velocityY * Time.smoothDeltaTime);
            //Debug.Log("UP: x: " + bodyPosition.x + ", y: " + bodyPosition.y);
		} else {
            bodyPosition.y -= velocityY * Time.smoothDeltaTime;
            //Debug.Log("DOWN, value of move: " + velocityY * Time.smoothDeltaTime);
            //Debug.Log("DOWN: x: " + bodyPosition.x + ", y: " + bodyPosition.y);
        }

		if (right) {
            bodyPosition.x += velocityX * Time.smoothDeltaTime;
            //Debug.Log("RIGHT, value of move: " + velocityX * Time.smoothDeltaTime);
            //Debug.Log("RIGHT: x: " + bodyPosition.x + ", y: " + bodyPosition.y);
        } else {
            bodyPosition.x -= velocityX * Time.smoothDeltaTime;
            //Debug.Log("LEFT, value of move: " + velocityX * Time.smoothDeltaTime);
            //Debug.Log("LEFT: x: " + bodyPosition.x + ", y: " + bodyPosition.y);
        }

        //transform.position = bodyPosition;
		GetComponent<Rigidbody2D> ().MovePosition (bodyPosition);
        //Debug.Log("End position -------------, x: " + GetComponent<Rigidbody2D>().position.x + ", y: " + GetComponent<Rigidbody2D>().position.y);
    }

	void OnCollisionEnter2D() {
		GetComponent<Rigidbody2D> ().freezeRotation = false;
	}
}
