﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverScript : MonoBehaviour {

    public TextMeshProUGUI score;

	// Use this for initialization
	void Start () {
        //score.SetText(score.GetParsedText() + PlayerPrefs.GetInt("Score"));
        score.SetText("Your score: " + PlayerPrefs.GetInt("Score"));
    }

    // Update is called once per frame
    void Update () {
		
	}
}
