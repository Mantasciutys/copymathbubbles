﻿using UnityEngine;
using System.Collections;

public class RandomTime : MonoBehaviour
{

    public Sprite sprite;
    private int additionalTime;
    private float velocityX, velocityY;
    private bool right, up;
    private float startX, startY;
    private GUIStyle style;
    private GUIContent content;
    private Rect textArea;

    private System.Random random;

    public int getAdditionalTime()
    {
        return additionalTime;
    }

    // Use this for initialization
    void Awake()
    {
        Debug.Log("Awake random time called");
        random = new System.Random();
        additionalTime = random.Next(5, 15);

        CreateDirections();
        CreatePositions();

        Debug.Log("Start pos x: " + startX);
        Debug.Log("Start pos y: " + startY);

        CreateVelocity();
    }

    // Use this for initialization
    void Start()
    {
       /* Debug.Log("Awake random time called");
        random = new System.Random();
        additionalTime = random.Next(5, 15);

        CreateDirections();
        CreatePositions();
        CreateVelocity();
        */
    }

    void OnGUI()
    {
        style = new GUIStyle();
        content = new GUIContent(additionalTime.ToString());


        Vector2 tempRect = transform.position;
        tempRect.x = Mathf.Round(tempRect.x * 100f) / 100f;
        tempRect.y = -transform.position.y;
        tempRect.y = Mathf.Round(tempRect.y * 100f) / 100f;

        style.fontSize = 20;

        Vector2 pos = Camera.main.WorldToScreenPoint(tempRect);

        Vector2 styleSize = style.CalcSize(content);
        pos.x -= styleSize.x / 2;
        pos.y -= styleSize.y / 2;
        textArea.position = pos;
        style.normal.textColor = Color.black;
        GUI.Label(textArea, content, style);
    }

    private void CreateVelocity()
    {
        velocityX = Random.Range(Con.Constants.VELOCITY_TIME_MIN, Con.Constants.VELOCITY_TIME_MAX);
        velocityY = Random.Range(Con.Constants.VELOCITY_TIME_MIN, Con.Constants.VELOCITY_TIME_MAX);
    }

    public void CreatePositions()
    {
        int chooseDirections = random.Next(1, 4);
        // UP
        if (chooseDirections == 1)
        {
            startY = 0.8f;
            startX = Random.Range(0f, 1f);
            // Go DOWN and (LEFT or RIGHT)
            up = false;
            int rightOrLeft = random.Next(1, 2);
            if (rightOrLeft == 1)
            {
                right = false;
            }
            else
            {
                right = true;
            }
        }
        // DOWN
        else if (chooseDirections == 2)
        {
            startY = 0.2f;
            startX = Random.Range(0f, 1f);
            // Go UP and (LEFT or RIGHT)
            up = true;
            int rightOrLeft = random.Next(1, 2);
            if (rightOrLeft == 1)
            {
                right = false;
            }
            else
            {
                right = true;
            }
        }
        // LEFT
        else if (chooseDirections == 3)
        {
            startX = 0.2f;
            startY = Random.Range(0f, 1f);
            // Go RIGHT and (UP or DOWN)
            right = true;
            int upOrDown = random.Next(1, 2);
            if (upOrDown == 1)
            {
                up = false;
            }
            else
            {
                up = true;
            }
        }
        // RIGHT
        else
        {
            startX = 0.8f;
            startY = Random.Range(0f, 1f);
            // Go LEFT and (UP or DOWN)
            right = false;
            int upOrDown = random.Next(1, 2);
            if (upOrDown == 1)
            {
                up = false;
            }
            else
            {
                up = true;
            }
        }
        Vector2 position = new Vector2();
        position.x = 0.67f;
        position.y = 0.785f;
        Vector2 worldPos = position;// Camera.main.ViewportToWorldPoint(position);
        Debug.Log("(Random) World pos x: " + worldPos.x);
        Debug.Log("(Random) World pos y: " + worldPos.y);
        transform.position = worldPos;

    }

    private void CreateDirections()
    {
        if (random.Next(2) == 0)
        {
            right = true;
        }
        else
        {
            right = false;
        }

        if (random.Next(2) == 0)
        {
            up = true;
        }
        else
        {
            up = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 bodyPosition = transform.position;
        Vector2 viewportPoint = Camera.main.WorldToViewportPoint(bodyPosition);

        if (up)
        {
            bodyPosition.y += velocityY * Time.smoothDeltaTime;
        }
        else
        {
            bodyPosition.y -= velocityY * Time.smoothDeltaTime;
        }

        if (right)
        {
            bodyPosition.x += velocityX * Time.smoothDeltaTime;
        }
        else
        {
            bodyPosition.x -= velocityX * Time.smoothDeltaTime;
        }


        // GetComponent<Rigidbody2D>().MovePosition(bodyPosition);
        transform.position = bodyPosition;
    }
}
